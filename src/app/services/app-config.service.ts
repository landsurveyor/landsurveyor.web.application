import { Inject, Injectable } from '@angular/core'
import { HttpClient, HttpParams } from "@angular/common/http"
import { map, take } from "rxjs/operators"
import { WEB_SERVICE_CONFIG } from "../models/provider-name-token"
import { WebServiceConfig } from "../models/web-service-config"
import { ResourceConfig } from "../models/resource-config"
import { Observable } from "rxjs"
import { UserConfig } from "../models/user-config"

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {

  appName: string = null
  user: UserConfig = null
  resources: ResourceConfig = null

  constructor(private http: HttpClient,
              @Inject(WEB_SERVICE_CONFIG) private webServiceConfig: WebServiceConfig) {
  }

  public getAppConfig(params: HttpParams): Observable<any> {
    return this.http.get<any>(this.webServiceConfig.appConfigUrl, { params })
    .pipe(take(1))
    .pipe(map((configResponse) => {
      this.appName = configResponse['app_name']
      this.user = new UserConfig(configResponse['user'])
      this.resources = new ResourceConfig(configResponse['resources'])
      return configResponse
    }))
  }
}
