import { Inject, Injectable } from '@angular/core'
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Observable, throwError } from 'rxjs'
import { IEmployee } from '../models/IEmployee'
import { catchError, retry } from 'rxjs/operators'
import { WEB_SERVICE_CONFIG } from "../models/provider-name-token"
import { WebServiceConfig } from "../models/web-service-config"

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient,
              @Inject(WEB_SERVICE_CONFIG) private webServiceConfig: WebServiceConfig) {
  }

  handleError(error: HttpErrorResponse) {
    return throwError(error)
  }

  getEmployees(): Observable<IEmployee[]> {
    return this.http.get<IEmployee[]>(this.webServiceConfig.webServiceUrl + '/api/v1/employees')
    .pipe(retry(2), catchError(this.handleError))
  }
}
