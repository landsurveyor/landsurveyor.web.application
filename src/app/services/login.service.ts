import { Inject, Injectable } from '@angular/core'
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Observable, throwError } from 'rxjs'
import { catchError, retry } from 'rxjs/operators'
import { WEB_SERVICE_CONFIG } from "../models/provider-name-token"
import { WebServiceConfig } from "../models/web-service-config"

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient,
              @Inject(WEB_SERVICE_CONFIG) private webServiceConfig: WebServiceConfig) {
  }

  handleError(error: HttpErrorResponse) {
    return throwError(error)
  }

  login(body: any): Observable<any> {
    return this.http.post(this.webServiceConfig.webServiceUrl + '/api/v1/login', body)
    .pipe(retry(2), catchError(this.handleError))
  }
}
