import { Inject, Injectable } from '@angular/core'
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http'
import { Observable, throwError } from 'rxjs'
import { catchError, retry } from 'rxjs/operators'
import { IDeed, IDeedCounter } from '../models/IDeed'
import { WEB_SERVICE_CONFIG } from "../models/provider-name-token"
import { WebServiceConfig } from "../models/web-service-config"

@Injectable({
  providedIn: 'root'
})
export class DeedService {

  constructor(private http: HttpClient,
              @Inject(WEB_SERVICE_CONFIG) private webServiceConfig: WebServiceConfig) {
  }

  handleError(error: HttpErrorResponse) {
    return throwError(error)
  }

  getDeeds(params?: HttpParams): Observable<IDeed[]> {
    return this.http.get<IDeed[]>(this.webServiceConfig.webServiceUrl + '/api/v1/deeds', { params })
    .pipe(retry(2), catchError(this.handleError))
  }

  getDeedById(deedId: number): Observable<IDeed> {
    return this.http.get<IDeed>(this.webServiceConfig.webServiceUrl + '/api/v1/deeds/' + deedId)
    .pipe(retry(2), catchError(this.handleError))
  }

  updateDeedById(deedId: number, body: any) {
    return this.http.patch<any>(this.webServiceConfig.webServiceUrl + '/api/v1/deeds/' + deedId, body)
    .pipe(retry(2), catchError(this.handleError))
  }

  getDeedsCounter(): Observable<IDeedCounter> {
    return this.http.get<IDeedCounter>(this.webServiceConfig.webServiceUrl + '/api/v1/deeds/counter')
    .pipe(retry(2), catchError(this.handleError))
  }
}
