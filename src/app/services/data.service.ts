import { Injectable } from '@angular/core'
import { BehaviorSubject } from 'rxjs'
import { IDeed } from '../models/IDeed'

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private deedSubject = new BehaviorSubject(null)
  public deedList = this.deedSubject.asObservable()

  constructor() {
  }

  addDeedList(deedList: IDeed[]): void {
    this.deedSubject.next(deedList)
  }
}
