import { IEmployee } from './IEmployee';
import { ICustomer } from './ICustomer';

export interface IDeed {
  deed_id: number;
  customer: ICustomer;
  employee?: IEmployee;
  deed_name: string;
  image_path?: string[];
  address: string;
  latitude: number;
  longitude: number;
  area?: number;
  deed_number?: string;
  deed_type?: IDeedType;
  status_id: number;
  created_date: Date;
  updated_date?: Date;
}

export interface IDeedCounter {
  total: number;
  waiting: number;
  assigned: number;
  collecting: number;
  success: number;
  cancelled: number;
}

export interface IDeedType {
  deed_type_id: number;
  name: string;
}

export enum DeedStatus {
  WAITING = 1,
  ASSIGNED = 2,
  COLLECTING = 3,
  SUCCESS = 4,
  CANCELLED = 5
}
