export interface ICustomer {
  customer_id: number;
  user_id: number;
  first_name: string;
  last_name: string;
  phone_number: string;
}
