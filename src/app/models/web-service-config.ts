export interface WebServiceConfig {
  webServiceUrl?: string
  appConfigUrl?: string
}
