export interface IEmployee {
  employee_id: number;
  first_name: string;
  last_name: string;
  user_id: number;
}
