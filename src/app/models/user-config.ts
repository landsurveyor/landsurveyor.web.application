export class UserConfig {
  get user(): UserAttributes {
    return this.userAttributes
  }

  constructor(private userAttributes: UserAttributes) {
  }
}

export interface UserAttributes {
  employee_id: number
  user_id: number
  first_name: string
  last_name: string
}
