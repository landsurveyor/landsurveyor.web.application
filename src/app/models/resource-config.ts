export class ResourceConfig {
  get resources(): ResourcesAttributes {
    return this.resourcesAttributes
  }

  constructor(private resourcesAttributes: ResourcesAttributes) {
  }
}

export interface ResourcesAttributes {
  deeds: Url
  employees: Url
  status: Status[]
}

export interface Url {
  url: string
}

export interface Status {
  status_id: number
  name: string
}
