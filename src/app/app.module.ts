import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { en_US, NgZorroAntdModule, NZ_I18N } from 'ng-zorro-antd';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { LoginComponent } from './pages/login/login.component';
import { LayoutComponent } from './pages/layout/layout.component';
import { MapComponent } from './pages/layout/map/map.component';
import { RequestsListComponent } from './pages/layout/requests-list/requests-list.component';
import { RequestComponent } from './pages/layout/request/request.component';
import { AreaPipe } from './shared/metric/area.pipe';
import { RequestModalComponent } from './pages/layout/request/request-modal.component';
import { StatusColorPipe } from './shared/status-color.pipe';
import { ImageModalComponent } from './pages/layout/request/image-modal.component';
import { environment } from "../environments/environment";
import { WEB_SERVICE_CONFIG } from "./models/provider-name-token";

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    LoginComponent,
    MapComponent,
    RequestsListComponent,
    RequestComponent,
    RequestModalComponent,
    AreaPipe,
    StatusColorPipe,
    ImageModalComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgZorroAntdModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule
  ],
  providers: [
    { provide: NZ_I18N, useValue: en_US },
    { provide: WEB_SERVICE_CONFIG, useValue: environment }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    RequestModalComponent,
    ImageModalComponent
  ]
})
export class AppModule {
}
