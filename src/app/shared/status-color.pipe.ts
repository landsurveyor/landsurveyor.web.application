import { Pipe, PipeTransform } from '@angular/core'
import { DeedStatus } from '../models/IDeed'

@Pipe({
  name: 'statusColor'
})
export class StatusColorPipe implements PipeTransform {

  transform(statusId: number): string {
    let color: string
    switch (statusId) {
      case DeedStatus.WAITING:
        color = ''
        break
      case DeedStatus.ASSIGNED:
        color = 'blue'
        break
      case DeedStatus.COLLECTING:
        color = 'cyan'
        break
      case DeedStatus.SUCCESS:
        color = 'green'
        break
      case DeedStatus.CANCELLED:
        color = 'red'
        break
      default:
    }
    return color
  }
}
