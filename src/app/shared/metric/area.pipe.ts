import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'area'
})
export class AreaPipe implements PipeTransform {

  transform(area: number): string {
    let extension = ' sq m'
    if (area >= 10000) {
      area = area / 1000000
      extension = ' sq km'
    }
    return area.toFixed(4) + extension
  }

}
