import { Component, OnDestroy, OnInit } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { LoginService } from '../../services/login.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm: FormGroup
  userId: number
  isInvalid: boolean

  constructor(private loginService: LoginService,
              private router: Router) {
  }

  ngOnInit(): void {
    if (localStorage.getItem('userId') != null) {
      this.router.navigate(['/'])
      this.isInvalid = false
    }
    this.loginForm = new FormGroup({
      username: new FormControl('admin', Validators.required),
      password: new FormControl('admin', Validators.required),
      role: new FormControl('admin', Validators.required)
    });
  }

  submitForm(): void {
    this.markFormDirty()
    if (this.loginForm.valid) {
      const requestBody = this.loginForm.value
      this.loginService.login(requestBody)
      .subscribe(data => {
        this.userId = data.user_id
        localStorage.setItem('userId', String(this.userId))
        this.router.navigate(['/'])
      }, () => {
        this.isInvalid = true
      })
    }
  }

  markFormDirty(): void {
    for (const i in this.loginForm.controls) {
      this.loginForm.controls[i].markAsDirty()
      this.loginForm.controls[i].updateValueAndValidity()
    }
  }

  ngOnDestroy(): void {
    this.loginForm.reset()
  }
}
