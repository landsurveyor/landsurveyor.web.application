import { Component, ElementRef, OnInit, ViewChild } from '@angular/core'
import { loadModules, setDefaultOptions } from 'esri-loader'
import { DataService } from '../../../services/data.service'
import { ActivatedRoute, Router } from '@angular/router'
import { IDeed } from '../../../models/IDeed'

export interface IPointMinMax {
  xMin: number
  yMin: number
  xMax: number
  yMax: number
  spatialReference: object
}

@Component({
  selector: 'app-maps',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})

export class MapComponent implements OnInit {
  @ViewChild('mapElement', { static: true })
  private mapElement: ElementRef<HTMLDivElement>
  private map: any = null
  private mapCenter: number[]
  private mapZoom: number

  constructor(private router: Router,
              private route: ActivatedRoute,
              private dataService: DataService) {
    (window as any).app = this
  }

  ngOnInit() {
    setDefaultOptions({ version: '3.31', css: true })
    this.mapCenter = [100.50144, 13.75398]
    this.mapZoom = 6
    this.createMap()
    this.route.queryParams.subscribe(params => {
      if (Object.keys(params).length !== 0 && this.map) {
        const lat = params.lat
        const lon = params.lon
        const z = params.z
        this.centerAndZoom(lat, lon, z)
      }
    })
  }

  async createMap(): Promise<void> {
    const [Map] = await loadModules(['esri/map'])
    this.map = new Map(this.mapElement.nativeElement, {
      basemap: 'streets-relief-vector',
      center: this.mapCenter,
      zoom: this.mapZoom,
      minZoom: 3,
      sliderPosition: 'top-right'
    })
    this.map.on('load', () => this.mapReady())
  }

  mapReady(): void {
    this.map.on('extent-change', () => this.onMapExtentChange())
    this.dataService.deedList.subscribe(data => {
      if (data) {
        this.clearGraphicsLayer()
        this.addGraphicFromData(data)
      }
    })
  }

  async addGraphicFromData(data: any): Promise<void> {
    const [Graphic, PictureMarkerSymbol, Point] = await loadModules([
      'esri/graphic',
      'esri/symbols/PictureMarkerSymbol',
      'esri/geometry/Point'
    ])
    const graphicList: Array<any> = new Array<any>()
    const pointList: Array<any> = new Array<any>()
    data.forEach((value: IDeed) => {
      const pictureMarkerSymbol = new PictureMarkerSymbol('/assets/images/pin-' + value.status_id + '.png', 32, 32)
      pictureMarkerSymbol.setOffset(0, 15)
      const point = new Point(value.longitude, value.latitude)
      pointList.push(point)
      graphicList.push(new Graphic(point, pictureMarkerSymbol))
    })
    if (pointList.length > 0) {
      const pointMinMax = this.findPointMinMax(pointList)
      await this.setMapExtent(
        pointMinMax.xMin,
        pointMinMax.yMin,
        pointMinMax.xMax,
        pointMinMax.yMax,
        pointMinMax.spatialReference)
      setTimeout(() => {
        pointList.length == 1 ? this.setMapZoom(13) : this.setMapZoom(this.getMapZoom() - 1)
      }, 800)
    }
    graphicList.forEach(graphic => {
      this.addGraphicToMapGraphicLayer(graphic)
    })
  }

  addGraphicToMapGraphicLayer(graphic: any): void {
    this.map.graphics.add(graphic)
  }

  clearGraphicsLayer(): void {
    if (this.map) {
      this.map.graphics.clear()
    }
  }

  async centerAndZoom(latitude: number, longitude: number, zoom: number = 6): Promise<void> {
    const [Point] = await loadModules(['esri/geometry/Point'])
    await this.map.centerAndZoom(new Point(longitude, latitude), zoom)
  }

  centerAt(mapPoint: any): void {
    this.map.centerAt(mapPoint)
  }

  resize(): void {
    this.map.resize()
    this.map.reposition()
  }

  getMapCenter(): any {
    return this.map.extent.getCenter()
  }

  getMapZoom(): number {
    return this.map.getZoom()
  }

  setMapZoom(zoom: number): void {
    this.map.setZoom(zoom)
  }

  onMapExtentChange(): any {
    this.router.navigate([], { queryParams: {} })
  }

  findPointMinMax(pointArray: any[]): IPointMinMax {
    const pointMinMax: IPointMinMax = {
      xMin: pointArray[0].x,
      yMin: pointArray[0].y,
      xMax: pointArray[0].x,
      yMax: pointArray[0].y,
      spatialReference: pointArray[0].spatialReference
    }

    pointMinMax.xMin = pointArray[0].x
    pointMinMax.yMin = pointArray[0].y
    pointMinMax.xMax = pointArray[0].x
    pointMinMax.yMax = pointArray[0].y
    pointMinMax.spatialReference = pointArray[0].spatialReference

    pointArray.forEach(value => {
      pointMinMax.xMin = (value.x < pointMinMax.xMin) ? value.x : pointMinMax.xMin
      pointMinMax.yMin = (value.y < pointMinMax.yMin) ? value.y : pointMinMax.yMin
      pointMinMax.xMax = (value.x > pointMinMax.xMax) ? value.x : pointMinMax.xMax
      pointMinMax.yMax = (value.y > pointMinMax.yMax) ? value.y : pointMinMax.yMax
    })
    return pointMinMax;
  }

  async setMapExtent(xMin: number, yMin: number, xMax: number, yMax: number, spatialReference: any): Promise<void> {
    const [Extent] = await loadModules(['esri/geometry/Extent'])
    this.map.setExtent(new Extent(xMin, yMin, xMax, yMax, spatialReference))
  }
}
