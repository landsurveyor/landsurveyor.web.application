import { Component, OnInit, ViewChild } from '@angular/core'
import { EmployeeService } from '../../services/employee.service'
import { Router } from '@angular/router'
import { MapComponent } from './map/map.component'
import { AppConfigService } from "../../services/app-config.service"
import { HttpParams } from "@angular/common/http"
import { UserAttributes } from "../../models/user-config"

const colorList = ['#f56a00', '#7265e6', '#ffbf00', '#00a2ae']

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  @ViewChild('mapComponent', { static: false })
  private mapComponent: MapComponent

  isCollapsed: boolean
  isSpinning: boolean
  isError: boolean

  user: UserAttributes
  avatarName: string
  avatarColor: string

  constructor(private employeeService: EmployeeService,
              private router: Router,
              private appConfigService: AppConfigService) {
  }

  ngOnInit(): void {
    if (localStorage.getItem('userId') == null) {
      this.router.navigate(['/login'])
    }
    this.loadAppConfig()
  }

  loadAppConfig(): void {
    this.isCollapsed = false
    this.isSpinning = true
    this.isError = false
    let params: HttpParams = new HttpParams()
    params = params.append('user_id', localStorage.getItem('userId'))
    this.appConfigService.getAppConfig(params)
    .subscribe(() => {
      this.user = this.appConfigService.user.user
      this.setAvatarName()
      this.setAvatarColor()
      this.isSpinning = false
    }, () => {
      this.isError = true
      this.user = null
    })
  }

  setAvatarName(): void {
    const firstLetter = this.user.first_name.substr(0, 1)
    const lastLetter = this.user.last_name.substr(0, 1)
    this.avatarName = firstLetter + lastLetter
  }

  setAvatarColor(): void {
    this.avatarColor = colorList[this.user.first_name.charCodeAt(0) % 4]
  }

  triggerMenu(): void {
    const mapCenter = this.mapComponent.getMapCenter()
    this.isCollapsed = !this.isCollapsed
    this.mapComponent.resize()
    setTimeout(() => {
      this.mapComponent.centerAt(mapCenter)
    }, 300);
  }

  onCollapsedChange(): void {
    const mapCenter = this.mapComponent.getMapCenter()
    this.mapComponent.resize()
    setTimeout(() => {
      this.mapComponent.centerAt(mapCenter)
    }, 300);
  }

  logout(): void {
    localStorage.clear();
    this.router.navigate(['/login'])
  }
}
