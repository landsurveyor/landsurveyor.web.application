import { Component, Input, OnInit } from '@angular/core'
import { NzMessageService, NzModalRef } from 'ng-zorro-antd'
import { EmployeeService } from '../../../services/employee.service'
import { IEmployee } from '../../../models/IEmployee'
import { DeedService } from '../../../services/deed.service'
import { DeedStatus } from '../../../models/IDeed'

interface IDeedRequest {
  [key: string]: any
}

@Component({
  selector: 'app-nz-modal-custom-footer-component',
  template: `
    <nz-spin [nzSpinning]="isSpinning" [nzDelay]="500">
      <nz-select nzShowSearch nzAllowClear nzPlaceHolder="Select a employee" [(ngModel)]="employeeId">
        <nz-option *ngFor="let employee of employeeList" [nzLabel]="employee.first_name + ' ' + employee.last_name"
                   [nzValue]="employee.employee_id"></nz-option>
      </nz-select>
    </nz-spin>
    <div *nzModalFooter>
      <button nz-button nzType="default" (click)="destroyModal()">Cancel</button>
      <button nz-button nzType="primary" (click)="assignEmployee()">Save</button>
    </div>
  `,
  styles: ['nz-select {width: 10rem;}']
})
export class RequestModalComponent implements OnInit {
  @Input()
  deedId: number
  @Input()
  statusId: number
  @Input()
  employeeId: number

  isSpinning: boolean

  employeeList: IEmployee[]

  constructor(private modal: NzModalRef,
              private employeeService: EmployeeService,
              private deedService: DeedService,
              private messageService: NzMessageService) {
    this.loadEmployeeData()
  }

  ngOnInit(): void {
  }

  loadEmployeeData(): void {
    this.isSpinning = true
    this.employeeService.getEmployees()
    .subscribe(employees => {
      this.employeeList = employees
      this.isSpinning = false
    }, error => {
      console.log(error)
    })
  }

  assignEmployee(): void {
    let requestBody: IDeedRequest
    requestBody = { employee_id: this.employeeId }
    requestBody.employee_id = this.employeeId
    if (this.statusId === DeedStatus.WAITING) {
      requestBody.status_id = DeedStatus.ASSIGNED
    } else {
      requestBody.status_id = this.statusId;
    }
    this.deedService.updateDeedById(this.deedId, requestBody)
    .subscribe(() => {
      this.messageService.success('Assigned this request successfully')
      this.destroyModal()
    }, () => {
      this.messageService.error('An error has occurred. The request cannot be assigned.')
    })
  }

  destroyModal(): void {
    this.modal.destroy()
  }
}
