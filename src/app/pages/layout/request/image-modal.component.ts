import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-image-modal',
  template: `
      <img src="{{ imgUrl }}" width="100%" class="center">
  `,
  styles: ['.center {display: block; margin-left: auto; margin-right: auto;}']
})
export class ImageModalComponent implements OnInit {
  @Input() imgUrl: string;

  constructor() {
  }

  ngOnInit() {
  }

}
