import { Component, Inject, OnInit } from '@angular/core'
import { DeedService } from '../../../services/deed.service'
import { DeedStatus, IDeed } from '../../../models/IDeed'
import { ActivatedRoute } from '@angular/router'
import { HttpErrorResponse } from '@angular/common/http'
import { NzMessageService, NzModalService, NzResultStatusType } from 'ng-zorro-antd'
import { RequestModalComponent } from './request-modal.component'
import { IEmployee } from '../../../models/IEmployee'
import { ImageModalComponent } from './image-modal.component'
import { WEB_SERVICE_CONFIG } from "../../../models/provider-name-token"
import { WebServiceConfig } from "../../../models/web-service-config"
import { Status } from "../../../models/resource-config"
import { AppConfigService } from "../../../services/app-config.service"

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.scss']
})
export class RequestComponent implements OnInit {
  isLoading: boolean
  isError: boolean
  resultStatus: NzResultStatusType
  resultMessage: string
  editable: boolean

  deed: IDeed
  statusList: Status[] = []

  host: string

  constructor(private route: ActivatedRoute,
              private deedService: DeedService,
              private modalService: NzModalService,
              private messageService: NzMessageService,
              private appConfigService: AppConfigService,
              @Inject(WEB_SERVICE_CONFIG) private webServiceConfig: WebServiceConfig) {
  }

  ngOnInit(): void {
    this.host = this.webServiceConfig.webServiceUrl
    this.loadRequestData()
    this.statusList = this.appConfigService.resources.resources.status
  }

  loadRequestData(): void {
    this.isLoading = true
    this.isError = false
    this.editable = false
    const deedId = +this.route.snapshot.paramMap.get('id')
    this.deedService.getDeedById(deedId)
    .subscribe(deed => {
      this.deed = deed
      this.editable = this.setEditable(deed.status_id)
      this.isLoading = false
    }, (error: HttpErrorResponse) => {
      this.isLoading = false
      this.isError = true
      this.deed = null
      if (error.status === 404) {
        this.resultStatus = '404'
        this.resultMessage = 'Sorry, the page you visited does not exist.'
      } else {
        this.resultStatus = '500'
        this.resultMessage = 'Sorry, there is an error on server.'
      }
    });
  }

  setEditable(statusId: number): boolean {
    let editable: boolean
    switch (statusId) {
      case DeedStatus.WAITING:
      case DeedStatus.ASSIGNED:
        editable = true
        break
      default:
        editable = false
    }
    return editable
  }

  createModal(): void {
    const modal = this.modalService.create({
      nzTitle: 'Assign request to employee',
      nzContent: RequestModalComponent,
      nzComponentParams: {
        deedId: this.deed.deed_id,
        statusId: this.deed.status_id,
        employeeId: this.deed.employee as IEmployee ? this.deed.employee.employee_id : null
      }
    });
    modal.afterClose.subscribe(() => this.loadRequestData())
  }

  createConfirmModal(): void {
    const modal = this.modalService.confirm({
      nzTitle: 'Confirmation',
      nzContent: 'Are you sure you want to cancel this request? This action cannot be undone and you will be able to recover any data.',
      nzOkText: 'OK',
      nzCancelText: 'Cancel',
      nzOnOk: () => {
        this.deedService.updateDeedById(this.deed.deed_id, { status_id: DeedStatus.CANCELLED })
        .subscribe(() => {
          this.messageService.success('Cancel this request successfully')
        }, () => {
          this.messageService.error('An error has occurred. The request cannot be cancel.')
        })
      }
    })
    modal.afterClose.subscribe(() => this.loadRequestData())
  }

  createImageModal(imagePath: string): void {
    const modal = this.modalService.create({
      nzContent: ImageModalComponent,
      nzClosable: false,
      nzFooter: [
        {
          label: 'Close',
          type: 'primary',
          onClick: () => modal.destroy()
        }
      ],
      nzWrapClassName: 'vertical-center-modal',
      nzComponentParams: {
        imgUrl: this.host + imagePath
      },
      nzOnOk: () => {
        modal.destroy()
      }
    })
  }
}
