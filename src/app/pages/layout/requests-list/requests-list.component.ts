import { Component, OnInit } from '@angular/core'
import { IDeed, IDeedCounter } from '../../../models/IDeed'
import { DeedService } from '../../../services/deed.service'
import { forkJoin } from 'rxjs'
import { DataService } from '../../../services/data.service'
import { Router } from '@angular/router'
import { HttpErrorResponse, HttpParams } from '@angular/common/http'
import { AppConfigService } from "../../../services/app-config.service"
import { Status } from "../../../models/resource-config"

@Component({
  selector: 'app-requests-list',
  templateUrl: './requests-list.component.html',
  styleUrls: ['./requests-list.component.scss']
})
export class RequestsListComponent implements OnInit {
  isLoading: boolean
  isTableLoading: boolean
  deedCounter: IDeedCounter
  deedList: IDeed[] = []
  status: number
  statusList: Status[] = []

  constructor(private deedService: DeedService,
              private dataService: DataService,
              private router: Router,
              private appConfigService: AppConfigService) {
  }

  ngOnInit(): void {
    this.loadRequestData()
    this.status = 0
    this.statusList = this.appConfigService.resources.resources.status
  }

  loadRequestData(): void {
    this.isLoading = true
    let params = new HttpParams()
    params = params.append('sort', 'status_id,updated_date')
    forkJoin(
      this.deedService.getDeedsCounter(),
      this.deedService.getDeeds(params)
    ).subscribe(data => {
      this.deedCounter = data[0]
      this.deedList = data[1]
      this.dataService.addDeedList(data[1])
      this.isLoading = false
    });
  }

  addLocationQueryParams(latitude: number, longitude: number, zoom?: number): void {
    this.router.navigate([], {
      queryParams: {
        lat: latitude,
        lon: longitude,
        z: zoom
      },
      queryParamsHandling: 'merge',
      skipLocationChange: true
    });
  }

  setFilter() {
    let params: HttpParams
    if (this.status === 0) {
      params = new HttpParams()
      params = params.append('sort', 'status_id,updated_date')
    } else {
      params = new HttpParams()
      params = params.append('status_id', this.status.toString())
      params = params.append('sort', 'updated_date')
    }
    this.isTableLoading = true;
    this.deedService.getDeeds(params)
    .subscribe(data => {
      this.isTableLoading = false
      this.deedList = data
      this.dataService.addDeedList(data)
    }, (error: HttpErrorResponse) => {
      this.isTableLoading = false
      this.deedList = []
      this.dataService.addDeedList([])
    })
  }
}
